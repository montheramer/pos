# PROJECT NAME:
   Point of Sale



## Description:
this project was made to help cashers to return back the exact ammount of money to customers 

## Technologies:

- [Angular 6](https://angular.io/)  
- [Nodejs express](https://expressjs.com/)
- [mysql](https://www.mysql.com/)

## Installation:

- install all dependencies found in 'package.json' file `$ npm install`.
- Run `ng build`.
- Run server `$ npm start`.

## Deployment:
 
 you can run this app on heroku

 
## Auther:
- [Monther Amer](https://bitbucket.org/montheramer) 
