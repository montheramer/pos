import { BrowserModule } from '@angular/platform-browser';
import { NgModule }         from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { ShowTbyCComponent } from './show-tby-c/show-tby-c.component';
import { LoginComponent } from './login/login.component';
import { MainComponent } from './main/main.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { RouterModule,Routes} from '@angular/router';
@NgModule({
  declarations: [
    AppComponent,
    ShowTbyCComponent,
    LoginComponent,
    MainComponent,
    TransactionsComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot([
      {path: '', component: MainComponent, pathMatch:'full'},
      {
        path: 'login', 
        component: LoginComponent,
        children:[
         {
          path:'transactions',
          component:TransactionsComponent
         }
        ]
        
      },
      {path: 'showtransactions' , component:ShowTbyCComponent},
      {path: 'transactions' , component:TransactionsComponent},
      ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
