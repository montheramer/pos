import { Injectable } from '@angular/core';
import { HttpClient} from  '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private cid="a";
  constructor(private http: HttpClient) { }
  

  public setCid(id:string){
  	sessionStorage.setItem(this.cid,id);
  }

  public getCid(){
  	var mon=sessionStorage.getItem(this.cid);
  	return mon
  }

}
