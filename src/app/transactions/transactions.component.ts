import { Component, OnInit } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import axios from 'axios';
import {ApiService} from '../api.service';
@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit {
   

   bills=[50,20,10,5,1];	
   change='';
   private  contacts:  Array<object> = [];
   private id : number;
  constructor(private api:ApiService){ }  

  ngOnInit() {
    this.id = parseInt(this.api.getCid());
  }
 	

  calculate(value1,value2){
  	var cost=parseInt(value1);
  	var paid=parseInt(value2);
    if(cost>paid){
      alert("the cost is grater than you pay")
    }else{
    var dif=paid-cost;
    var obj={}
    var b=0;
    var str='';
    while(dif!==0){
      for(var i=0;i<this.bills.length;i++){
      if(dif>=this.bills[i]){
        b=this.bills[i];
        break;        
      }
    }
    dif=dif-b;
    if(obj[b]){
      obj[b]+=1;
    }else{
      obj[b]=1;
    }
    }
    for(var key in obj){
    str=str+'[ '+key+' ]';
    str=str+' : '+ obj[key]+' , ';
    }
    if(cost===paid){
    }else{
    this.change=str 
    }
    this.done(obj,paid);  
    }
  	
  }
  done(data,paid){
    var arr=[];
    var id=this.id;
    arr.push([id,'D',paid,1])
    for(var key in data){
      arr.push([id,'W',key,data[key]])
    }
  axios({
      method:'post',
      url:'/transactions/create',
      data:arr,
    })
    .then(function(response){
      console.log(response);
    })
    .catch(function(response){
      console.log(response)
    });

  }



}
