import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {ApiService} from '../api.service';
import axios from 'axios';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  private username: string;
  private id : string;
  public userInfo: UserInfo[];
  constructor(private router: Router,private api:ApiService) { }


  ngOnInit() {
    var that =this;
      axios({
      method:'get',
      url:'/user/retrive',
    })
    .then(function(response){
      that.userInfo=response.data
    })
    .catch(function(response){
    });
    this.id = this.api.getCid();
  }

  getId(name){
    var that=this
    this.id = this.api.getCid();
    for(var i=0;i<this.userInfo.length;i++){
      if(this.userInfo[i].email==name){
      this.router.navigate(["login/transactions"]);
      var userid=this.userInfo[i].id +"";
      this.api.setCid(userid);

      }
    }

     

  }
}

interface UserInfo {
  email:string;
  firstName:string;
  id: number;
  lastName: string;
}