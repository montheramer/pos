import { Component, OnInit } from '@angular/core';
import axios from 'axios';
@Component({
  selector: 'app-show-tby-c',
  templateUrl: './show-tby-c.component.html',
  styleUrls: ['./show-tby-c.component.css']
})
export class ShowTbyCComponent implements OnInit {
public data: CasherData[];
public cashInfo: CashInfo[];
  constructor() { }

  ngOnInit() {
  	var that = this
  	  axios({
      method:'get',
      url:'/user/retrive'
    })
    .then(function(response){
    	 that.data=response.data;
    })

  }

    selectionChange(id): void {
        var that=this
  	  axios({
      method:'get',
      url:`/transactions/retrive/${id}`
	    })
	    .then(function(response){
	      that.cashInfo = response.data;
	    })        
    }


}

interface CasherData {
	firstName: string;
	lastName: string;
	id: number;
	email: string;
}

interface CashInfo {
	ammount:number;
	bill:number;
	type: string;
	time: string;
	id: number;
	cashierId: number;
}