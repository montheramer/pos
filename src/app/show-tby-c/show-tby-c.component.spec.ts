import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowTbyCComponent } from './show-tby-c.component';

describe('ShowTbyCComponent', () => {
  let component: ShowTbyCComponent;
  let fixture: ComponentFixture<ShowTbyCComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowTbyCComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowTbyCComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
