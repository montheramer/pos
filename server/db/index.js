const mysql = require('mysql');
const connection = mysql.createConnection({
	host:'localhost',
	user:'root',
	password:'ABCdef_1234567890'
	//database:'myDB'
});

connection.connect(function(err){
	if(err){
		throw(err);
	}
	console.log('connected');
	connection.query("CREATE DATABASE IF NOT EXISTS myDB ",function(err,result){
		if(err) throw err;
		console.log("database created")
	})
	connection.query("USE myDB ",function(err,result){
		if(err) throw err;
		console.log("database myDB")
	})
	connection.query("CREATE TABLE IF NOT EXISTS cashier (id INT AUTO_INCREMENT PRIMARY KEY,firstName VARCHAR(20),lastName VARCHAR(20),email VARCHAR(30) NOT NULL UNIQUE)",function(err,res){
		if(err) throw err;
		console.log('table cashier created')
	})
	connection.query("CREATE TABLE IF NOT EXISTS transactions (id INT AUTO_INCREMENT PRIMARY KEY,time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,cashierId INT, type VARCHAR(20),bill INT,ammount INT,FOREIGN KEY (cashierId) REFERENCES cashier(id))",function(err,result){
		if(err) throw err;
		console.log('table transaction created');
	})

})

module.exports=connection;