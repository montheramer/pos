var userRouter=require('express').Router();
var userController = require('./userController');

userRouter.route('/retrive')
.get(function(req,res){
	userController.retrive(req,res);
})

userRouter.route('/create')
.post(function(req,res){
	userController.create(req,res);
})

module.exports=userRouter;
