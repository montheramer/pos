var transactionRouter=require('express').Router();
var transactionController = require('./transactionController');

transactionRouter.route('/create')
.post(function(req,res){
	transactionController.create(req,res);
})


transactionRouter.route('/retrive/:userid')
.get(function(req,res){
	transactionController.retrive(req,res);
})

module.exports=transactionRouter;