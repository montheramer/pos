var express = require('express');
var db = require('./db');
var bodyParser = require('body-parser');
var app=express();
var path = require('path')
app.use(express.static(__dirname + '/../dist/my-app'));

app.get('/',function(req,res){
	res.sendFile('index.html')
})
var userRouter=require('./resourses/users/userRouter');

var transactionRouter=require('./resourses/transactions/transactionRouter');
app.use(bodyParser.json());


app.use('/user',userRouter)
app.use('/transactions',transactionRouter);
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '../dist/my-app/index.html'));
});

module.exports=app;
